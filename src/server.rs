use std::error::Error;

use tokio::net::TcpListener;

use blind_maze::core::game;

use crate::client_handler::ClientHandler;
use crate::shutdown::ShutdownGuard;

/// Server for playing blind maze.
pub struct Server {
  shutdown_guard: ShutdownGuard,
}

impl Server {
  /// Constructs new server.
  pub fn new(shutdown_guard: ShutdownGuard) -> Self {
    Self { shutdown_guard }
  }

  /// Spawns TCP server and accept client connecions.
  /// Connections are then handled using ClientHandler.
  pub async fn run(&mut self, address: &str) -> Result<(), Box<dyn Error + Send + Sync>> {
    // Bind TCP server.
    let listener = TcpListener::bind(address).await?;
    log::info!("Server is listening on {}", address);

    // Accept connections and process them.
    log::info!("Waiting for client connections...");
    loop {
      tokio::select! {
        res = listener.accept() => {
          match res {
            Ok((stream, _address)) => {
              // Create new game.
              let game = match game::Game::new() {
                Err(_) => {
                  log::error!("Unable to create game!");
                  continue;
                }
                Ok(v) => v,
              };
              // Create handler.
              let mut handler = ClientHandler::new(
                stream,
                game
              )?;
              // Run client handler.
              tokio::spawn(async move {
                handler.run().await.unwrap_or_else(|err| {
                  log::error!("Error occured during handling connection: {}", err);
                });
              });
            }
            Err(e) => {
              log::error!("Invalid stream: {}", e);
            }
          }
        }
        _ = self.shutdown_guard.until_triggered() => { break; }
      }
    }

    Ok(())
  }
}
