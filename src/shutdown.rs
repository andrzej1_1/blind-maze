use std::error::Error;

use tokio::sync::mpsc;
use tokio::sync::watch;

type Dropable<T> = Option<T>;

pub struct ShutdownGuard {
  #[allow(dead_code)]
  parent_ack_tx: Option<mpsc::Sender<()>>, // used in child

  children_ack_tx: Dropable<mpsc::Sender<()>>, // used in child
  children_ack_rx: mpsc::Receiver<()>,         // used in parent
  trigger_tx: Dropable<watch::Sender<()>>,     // used in parent
  trigger_rx: watch::Receiver<()>,             // used in child
  is_triggered: bool,                          // for check usage without async
}

impl ShutdownGuard {
  pub fn new() -> Self {
    let children_ack_channel = mpsc::channel(1);
    let trigger_ack_channel = watch::channel(());
    ShutdownGuard {
      parent_ack_tx: None,
      children_ack_tx: Some(children_ack_channel.0),
      children_ack_rx: children_ack_channel.1,
      trigger_tx: Some(trigger_ack_channel.0),
      trigger_rx: trigger_ack_channel.1,
      is_triggered: false,
    }
  }

  pub fn new_child(&self) -> Self {
    let children_ack_channel = mpsc::channel(1);
    ShutdownGuard {
      parent_ack_tx: self.children_ack_tx.as_ref().cloned(), // attach to parent
      children_ack_tx: Some(children_ack_channel.0),
      children_ack_rx: children_ack_channel.1,
      trigger_tx: None,                    // no triggering possible in child
      trigger_rx: self.trigger_rx.clone(), // copy trigger handler
      is_triggered: self.is_triggered,
    }
  }

  // Waits until shutdown is completed by children
  pub async fn until_finished(&mut self) {
    // We drop our sender first because the recv() call otherwise
    // sleeps forever.
    self.children_ack_tx = None;

    // When every sender has gone out of scope, the recv call
    // will return with an error. We ignore the error.
    let _ = self.children_ack_rx.recv().await;
  }

  // Waits until shutdown is triggered
  pub async fn until_triggered(&mut self) -> Result<(), watch::error::RecvError> {
    self.trigger_rx.changed().await
  }

  pub fn trigger(&mut self) -> Result<(), Box<dyn Error>> {
    // triggering is only possible in root!
    match &self.trigger_tx {
      Some(ch) => {
        ch.send(())?;
        self.is_triggered = true;
        Ok(())
      }
      None => Err("Maze is too big!".into()),
    }
  }
}
