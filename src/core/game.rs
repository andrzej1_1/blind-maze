use std::error::Error;

use crate::core::maze::{Maze, MazeCoords, WorldDirection};

static SERVER_MAZE_WIDTH: u8 = 50;
static SERVER_MAZE_HEIGHT: u8 = 50;

pub struct Game {
  maze: Maze,
  player_coords: MazeCoords,
  player_direction: WorldDirection,
}

impl Game {
  pub fn new() -> Result<Self, Box<dyn Error>> {
    let maze = Maze::new(SERVER_MAZE_WIDTH, SERVER_MAZE_HEIGHT)?;
    log::debug!("New maze created:\n{:?}", maze.to_string());
    log::debug!("Starting at {:?}", maze.get_start());
    log::debug!("Ending at {:?}", maze.get_end());
    let player_coords = maze
      .get_start()
      .clone()
      .ok_or("Missing start field in maze")?;
    let player_direction: WorldDirection = rand::random();
    Ok(Game {
      maze,
      player_coords,
      player_direction,
    })
  }

  pub fn get_next_player_coords(&self) -> Option<MazeCoords> {
    let next_coords = match self.player_direction {
      WorldDirection::North => {
        if self.player_coords.get_y() == 0 {
          return None;
        }
        MazeCoords::new(self.player_coords.get_x(), self.player_coords.get_y() - 1)
      }
      WorldDirection::West => {
        if self.player_coords.get_x() == 0 {
          return None;
        }
        MazeCoords::new(self.player_coords.get_x() - 1, self.player_coords.get_y())
      }
      WorldDirection::East => {
        // todo make more bulletproof
        if self.player_coords.get_x() + 1 == 0 {
          return None;
        }
        MazeCoords::new(self.player_coords.get_x() + 1, self.player_coords.get_y())
      }
      WorldDirection::South => {
        // todo make more bulletproof
        if self.player_coords.get_y() + 1 == 0 {
          return None;
        }
        MazeCoords::new(self.player_coords.get_x(), self.player_coords.get_y() + 1)
      }
    };
    Some(next_coords)
  }

  pub fn handle_cmd(&mut self, cmd: &str) -> &str {
    match cmd {
      "look" => match self.maze.get_cell_by_coords(&self.player_coords) {
        Err(_) => "internal error\n",
        Ok(current_cell) => {
          log::trace!(
            "Looking toward {:?} at {:?}",
            self.player_direction,
            self.player_coords
          );
          let is_wall = match self.player_direction {
            WorldDirection::North => current_cell.has_wall_top(),
            WorldDirection::West => current_cell.has_wall_left(),
            WorldDirection::East => current_cell.has_wall_right(),
            WorldDirection::South => current_cell.has_wall_bottom(),
          };
          if is_wall {
            return "wall\n";
          }
          "path\n"
        }
      },
      "step" => {
        match self.maze.get_cell_by_coords(&self.player_coords) {
          Err(_) => "internal error\n",
          Ok(current_cell) => {
            let is_wall = match self.player_direction {
              WorldDirection::North => current_cell.has_wall_top(),
              WorldDirection::West => current_cell.has_wall_left(),
              WorldDirection::East => current_cell.has_wall_right(),
              WorldDirection::South => current_cell.has_wall_bottom(),
            };
            if is_wall {
              return "error: wall ahead\n";
            }
            match self.get_next_player_coords() {
              None => "error: limit exceeded\n",
              Some(next_coords) => {
                match self.maze.get_cell_by_coords(&next_coords) {
                  Err(_) => {
                    // we should not allow to get out of the board.
                    // walls should protect it!
                    "internal error\n"
                  }
                  Ok(_) => {
                    // note: we skip wall checking here, we assume it is consistent
                    // so just moving player positon
                    self.player_coords = next_coords;
                    log::trace!("Stepped into {:?}", self.player_coords);
                    // Check if game is finished
                    match &self.maze.get_end() {
                      None => {
                        return "internal error\n";
                      }
                      Some(end_coords) => {
                        if self.player_coords == *end_coords {
                          return "You found your way out of this dark maze! Here is your token: SECRET_TOKEN_HERE\n";
                        }
                      }
                    }
                    "ok\n"
                  }
                }
              }
            }
          }
        }
      }
      "turn left" => {
        self.player_direction = self.player_direction.get_left();
        log::trace!("Player turned left, now: {:?}", self.player_direction);
        "ok\n"
      }
      "turn right" => {
        self.player_direction = self.player_direction.get_right();
        log::trace!("Player turned right, now: {:?}", self.player_direction);
        "ok\n"
      }
      _ => "unknown command\n",
    }
  }

  /// Gets current player coords.
  pub fn get_player_coords(&self) -> &MazeCoords {
    &self.player_coords
  }

  /// Gets current player direction.
  pub fn get_player_direction(&self) -> &WorldDirection {
    &self.player_direction
  }
}
