use std::collections::HashSet;
use std::error::Error;
use std::fmt;

use rand::distributions::{Distribution, Standard};
use rand::seq::SliceRandom;
use rand::Rng;

use crate::core::utils::MultiLineBuffer;

#[derive(Clone)]
pub struct MazeCell {
  wall_top: bool,
  wall_left: bool,
  wall_right: bool,
  wall_bottom: bool,
}

impl MazeCell {
  pub fn new() -> Self {
    MazeCell {
      wall_left: true,
      wall_top: true,
      wall_right: true,
      wall_bottom: true,
    }
  }

  /// Whether has wall on left.
  pub fn has_wall_left(&self) -> bool {
    self.wall_left
  }

  /// Whether has wall on top.
  pub fn has_wall_top(&self) -> bool {
    self.wall_top
  }

  /// Whether has wall on right.
  pub fn has_wall_right(&self) -> bool {
    self.wall_right
  }

  /// Whether has wall on bottom.
  pub fn has_wall_bottom(&self) -> bool {
    self.wall_bottom
  }
}

#[derive(Eq, PartialEq, Hash, Debug, Clone)]
pub struct MazeCoords {
  x: u8,
  y: u8,
}

impl MazeCoords {
  pub fn new(x: u8, y: u8) -> Self {
    Self { x, y }
  }

  /// Gets X coordinate.
  pub fn get_x(&self) -> u8 {
    self.x
  }

  /// Gets Y coordinate.
  pub fn get_y(&self) -> u8 {
    self.y
  }
}

pub struct Maze {
  width: u8,
  height: u8,
  cells: Vec<MazeCell>,
  start: Option<MazeCoords>,
  end: Option<MazeCoords>,
}

impl Maze {
  pub fn new(width: u8, height: u8) -> Result<Self, Box<dyn Error>> {
    // Generate maze filled with walls only.
    let num_cells = match (width as usize).checked_mul(height as usize) {
      Some(x) => x,
      None => return Err("Maze is too big!".into()),
    };
    let mut m = Maze {
      width,
      height,
      cells: vec![MazeCell::new(); num_cells],
      start: None,
      end: None,
    };

    // TODO think about moving DFS outside! this would allow to get start/end in better way! we could move start/end to game itself!

    // Filling maze with roads using randomized DFS.
    // Algorithm:
    // 1. Choose the initial cell, mark it as visited and push it to the stack
    // 2. While the stack is not empty
    //    a. Pop a cell from the stack and make it a current cell
    //    b. If the current cell has any neighbours which have not been visited
    //       A) Push the current cell to the stack
    //       B) Choose one of the unvisited neighbours
    //       C) Remove the wall between the current cell and the chosen cell
    //       D) Mark the chosen cell as visited and push it to the stack
    let mut visited_cells = HashSet::new();
    let mut stack = vec![];

    let rng = rand::thread_rng();
    let initial_cell_coords = m.get_random_coords_inside(rng)?;
    visited_cells.insert(initial_cell_coords.clone());
    stack.push(initial_cell_coords.clone());

    // we store longest path to determine END of maze
    let mut longest_path = 0;
    let mut longest_path_coords = initial_cell_coords.clone();

    while let Some(current_cell_coords) = stack.pop() {
      // update longest path
      if stack.len() > longest_path {
        longest_path = stack.len();
        longest_path_coords = current_cell_coords.clone();
      }
      // default DFS logic
      let mut neighs = m.get_neighbours(&current_cell_coords);
      neighs.retain(|x| !visited_cells.contains(x));
      let unvisited = neighs;
      if let Some(next_cell_coords) = unvisited.choose(&mut rand::thread_rng()) {
        stack.push(current_cell_coords.clone());
        m.remove_wall_between(&current_cell_coords, next_cell_coords)?;
        visited_cells.insert(next_cell_coords.clone());
        stack.push(next_cell_coords.clone());
      }
    }

    // update maze start/end
    m.start = Some(initial_cell_coords);
    m.end = Some(longest_path_coords);

    Ok(m)
  }

  pub fn get_cell_by_coords(
    &mut self,
    coords: &MazeCoords,
  ) -> Result<&mut MazeCell, Box<dyn Error>> {
    let cell_index = (coords.y as usize) * (self.width as usize) + (coords.get_x() as usize);
    match self.cells.get_mut(cell_index) {
      Some(c) => Ok(c),
      None => Err("Cell not found!".into()),
    }
  }

  pub fn get_random_coords_inside(
    &self,
    mut rng: rand::rngs::ThreadRng,
  ) -> Result<MazeCoords, Box<dyn Error>> {
    let rand_row = rng.gen_range(0..self.height);
    let rand_col = rng.gen_range(0..self.width);

    Ok(MazeCoords::new(rand_row as u8, rand_col as u8))
  }

  pub fn get_neighbours(&self, cell: &MazeCoords) -> Vec<MazeCoords> {
    let mut neighs = vec![];
    if cell.y > 0 {
      neighs.push(MazeCoords::new(cell.x, cell.y - 1));
    }
    if cell.x > 0 {
      neighs.push(MazeCoords::new(cell.x - 1, cell.y));
    }
    if cell.x < (self.width - 1) {
      neighs.push(MazeCoords::new(cell.x + 1, cell.y));
    }
    if cell.y < (self.height - 1) {
      neighs.push(MazeCoords::new(cell.x, cell.y + 1));
    }
    neighs
  }

  pub fn remove_wall_between(
    &mut self,
    coords_a: &MazeCoords,
    coords_b: &MazeCoords,
  ) -> Result<(), Box<dyn Error>> {
    {
      let cell_a: &mut MazeCell = self.get_cell_by_coords(coords_a)?;
      if coords_a.x > coords_b.x {
        cell_a.wall_left = false;
      } else if coords_a.x < coords_b.x {
        cell_a.wall_right = false;
      } else if coords_a.y > coords_b.y {
        cell_a.wall_top = false;
      } else if coords_a.y < coords_b.y {
        cell_a.wall_bottom = false;
      }
    }
    {
      let cell_b: &mut MazeCell = self.get_cell_by_coords(coords_b)?;
      if coords_a.x > coords_b.x {
        cell_b.wall_right = false;
      } else if coords_a.x < coords_b.x {
        cell_b.wall_left = false;
      } else if coords_a.y > coords_b.y {
        cell_b.wall_bottom = false;
      } else if coords_a.y < coords_b.y {
        cell_b.wall_top = false;
      }
    }
    Ok(())
  }

  /// Gets start coords.
  pub fn get_start(&self) -> &Option<MazeCoords> {
    &self.start
  }

  /// Gets end coords.
  pub fn get_end(&self) -> &Option<MazeCoords> {
    &self.end
  }
}

impl fmt::Display for Maze {
  fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
    // top left item
    print!("+");
    // handle cells
    let mut multi_buffer = MultiLineBuffer::new(2);
    for (i, cell) in self.cells.iter().enumerate() {
      // handle first row output
      if i < self.width.into() {
        print!("{}", (if cell.wall_top { "--+" } else { "  +" }));
        if i == (self.width - 1).into() {
          println!();
        }
      }
      // handle first column output
      if i % self.width as usize == 0 {
        let left_wall = if cell.wall_left { "|" } else { " " };
        if multi_buffer.append(vec![left_wall, "+"]).is_err() {
          return Err(fmt::Error);
        }
      }
      // handle default case
      let line_1 = if cell.wall_right { "  |" } else { "   " };
      let line_2 = if cell.wall_bottom { "--+" } else { "  +" };
      if multi_buffer.append(vec![line_1, line_2]).is_err() {
        return Err(fmt::Error);
      }
      if i % self.width as usize == (self.width as usize - 1) {
        multi_buffer.flush();
      }
    }
    Ok(())
  }
}

#[derive(Debug)]
pub enum WorldDirection {
  North,
  West,
  East,
  South,
}

impl WorldDirection {
  pub fn get_left(&self) -> Self {
    use WorldDirection::*;
    match *self {
      North => West,
      West => South,
      South => East,
      East => North,
    }
  }
  pub fn get_right(&self) -> Self {
    use WorldDirection::*;
    match *self {
      North => East,
      East => South,
      South => West,
      West => North,
    }
  }
}

impl Distribution<WorldDirection> for Standard {
  fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> WorldDirection {
    match rng.gen_range(0..=3) {
      0 => WorldDirection::North,
      1 => WorldDirection::West,
      2 => WorldDirection::East,
      _ => WorldDirection::South,
    }
  }
}

impl fmt::Display for WorldDirection {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match *self {
      WorldDirection::North => write!(f, "North"),
      WorldDirection::West => write!(f, "West"),
      WorldDirection::East => write!(f, "East"),
      WorldDirection::South => write!(f, "South"),
    }
  }
}
