use std::error::Error;

pub struct MultiLineBuffer {
  buffers: Vec<String>,
}

impl MultiLineBuffer {
  pub fn new(num_lines: usize) -> Self {
    MultiLineBuffer {
      buffers: vec![String::new(); num_lines],
    }
  }

  pub fn append(&mut self, lines: Vec<&str>) -> Result<(), Box<dyn Error>> {
    if lines.len() != self.buffers.len() {
      return Err("Invalid number of lines!".into());
    }
    for it in lines.iter().zip(self.buffers.iter_mut()) {
      let (lines_it, buf_it) = it;
      buf_it.push_str(lines_it);
    }
    Ok(())
  }

  pub fn flush(&mut self) {
    for buffer in self.buffers.iter_mut() {
      println!("{}", buffer);
      buffer.clear();
    }
  }
}
