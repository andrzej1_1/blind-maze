use std::process;

use tokio::signal;

use shutdown::ShutdownGuard;

mod client_handler;
mod server;
mod shutdown;

#[tokio::main]
async fn main() {
  // Preparation.
  pretty_env_logger::init();
  let mut shutdown_guard = ShutdownGuard::new();

  // Create server and run it asynchronously (both IPv4 and IPv6).
  let shutdown_guard_child = shutdown_guard.new_child();
  let mut server = server::Server::new(shutdown_guard_child);
  tokio::spawn(async move {
    let res = server.run("[::]:24365").await;
    if let Err(e) = res {
      log::error!("Error occured while running server: {}", e);
      process::exit(1);
    }
  });

  // wait for shutdown signal from user
  match signal::ctrl_c().await {
    Ok(()) => {}
    Err(err) => {
      log::error!("Unable to listen for shutdown signal: {}", err);
    }
  }

  // trigger shutdown and wait until finished
  log::info!("Triggering shutdown...");
  if let Err(e) = shutdown_guard.trigger() {
    log::error!("Error while triggering shutdown: {}", e);
  }
  log::info!("Waiting for graceful termination...");
  shutdown_guard.until_finished().await;

  log::info!("Server was stopped.");
}
