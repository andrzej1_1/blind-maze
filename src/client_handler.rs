use std::error::Error;

use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;

use blind_maze::core::game::Game;

/// Handler for client connection.
pub struct ClientHandler {
  /// Client connection.
  stream: TcpStream,
  /// Game with state.
  game: Game,
  /// Client ID.
  client_id: u32,
}

impl ClientHandler {
  /// Constructs new ClientHandler.
  pub fn new(stream: TcpStream, game: Game) -> Result<Self, Box<dyn Error + Send + Sync>> {
    // Port is considered as client ID.
    let client_id = stream.peer_addr()?.port().into();
    log::info!("Client {} connected.", client_id);

    Ok(Self {
      stream,
      game,
      client_id,
    })
  }

  pub async fn run(&mut self) -> Result<(), Box<dyn Error + Send + Sync>> {
    log::info!("Sending welcome message.");
    let welcome_msg = "Welcome to my maze!\n\n";
    match self.stream.write(welcome_msg.as_bytes()).await {
      Err(_) => {
        log::error!("Unable to send welcome message!");
      }
      Ok(_) => {}
    }

    let initial_state_msg = format!(
      "You are starting at {}x{} facing toward {}.\n",
      self.game.get_player_coords().get_x(),
      self.game.get_player_coords().get_y(),
      self.game.get_player_direction()
    );
    match self.stream.write(initial_state_msg.as_bytes()).await {
      Err(_) => {
        log::error!("Unable to send welcome message!");
      }
      Ok(_) => {}
    }
    let mut buffer: Vec<u8> = vec![0; 128];
    loop {
      match self.stream.read(&mut buffer).await {
        Ok(num_bytes) => {
          if num_bytes == 0 {
            log::info!("Client disconnected.");
            break;
          }
          log::trace!("Got {:} bytes", num_bytes);
          match std::str::from_utf8(&buffer[0..num_bytes]) {
            Err(_) => {
              log::warn!("Invalid data received, ignoring!");
            }
            Ok(command) => {
              let response = self.game.handle_cmd(command.trim_end());
              match self.stream.write(response.as_bytes()).await {
                Err(_) => {
                  log::error!("Unable to send response!");
                }
                Ok(_) => {
                  log::trace!("Command handled!");
                }
              }
            }
          }
        }
        Err(e) => {
          log::error!("Error while reading from stream: {:?}", e);
        }
      }
    }
    Ok(())
  }
}
